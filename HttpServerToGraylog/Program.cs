using System.Text.Json.Serialization;
using HttpServerToGraylog;
using Microsoft.AspNetCore.Mvc;
using Serilog.Events;

var builder = WebApplication.CreateSlimBuilder(args);

// add console log with format
builder.Logging.AddSimpleConsole(options =>
{
    options.SingleLine = true;
    options.TimestampFormat = "yy-MM-dd HH:mm:ss ";
});

builder.Services.ConfigureHttpJsonOptions(options =>
    {
        options.SerializerOptions.Converters.Add(new JsonStringEnumConverter());
        options.SerializerOptions.TypeInfoResolverChain.Insert(0, AppJsonSerializerContext.Default);
    })
    .AddHttpClient()
    .AddScoped<GelfHttpClient>()
    .AddHttpContextAccessor();

var app = builder.Build();

var idempotencyKeys = new HashSet<string>();

var serilogs = app.MapGroup("/serilogs");
serilogs.MapPost("/{key}",
    handler: async ([FromRoute] string key,
        [FromBody] LogEvent[]? events,
        [FromHeader(Name = "Idempotency-Key")] string? idempotencyKey,
        [FromHeader(Name = "X-Forwarded-For")] string? forwardedFor,
        [FromServices] IConfiguration configuration,
        [FromServices] GelfHttpClient gelfClient,
        [FromServices] ILogger<Program> logger,
        [FromServices] IHttpContextAccessor httpContextAccessor) =>
    {
        try
        {
            if (events == null || events.Length == 0) return Results.BadRequest();

            // check idempotency
            if (!string.IsNullOrEmpty(idempotencyKey) && !idempotencyKeys.Add(idempotencyKey))
            {
                logger.LogWarning("Idempotency key {key} already exists", idempotencyKey);
                return Results.Conflict();
            }
            
            // check key
            var source = configuration[$"Keys:{key}"];
            if (string.IsNullOrEmpty(source)) return Results.Unauthorized();

            var remoteIp = !string.IsNullOrEmpty(forwardedFor) 
                ? forwardedFor
                : httpContextAccessor.HttpContext?.Connection.RemoteIpAddress?.ToString();
            
            await gelfClient.SendAsync(source, remoteIp, events);
            return Results.Ok();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Failed to send log events");
        }
        return Results.Ok();
    });


app.Run();


[JsonSerializable(typeof(LogEvent[]))]
internal partial class AppJsonSerializerContext : JsonSerializerContext
{
}

/// <summary>
/// Represents a log event.
/// </summary>
public class LogEvent
{
    [JsonPropertyName("Timestamp")]
    public DateTimeOffset Timestamp { get; set; }

    [JsonPropertyName("Level")]
    public LogEventLevel Level { get; set; } = LogEventLevel.Information;

    [JsonPropertyName("MessageTemplate")]
    public string? MessageTemplate { get; set; }

    [JsonPropertyName("RenderedMessage")]
    public string? RenderedMessage { get; set; }

    [JsonPropertyName("Properties")]
    public Dictionary<string, object>? Properties { get; set; }

    [JsonPropertyName("Renderings")]
    public Dictionary<string, LogEventRendering[]>? Renderings { get; set; }

    [JsonPropertyName("Exception")]
    public string? Exception { get; set; }
    
}

public record LogEventRendering(
    string Format,
    string Rendering);